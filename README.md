# ВМ - Python - Рубежка 2 - 2017

#### Задание 1 (интерполяция)

- ##### метод Лагранжа
```
def Lagrange(x, points):
    res = 0
    n = len(points)
    for i in range(n):
        F = 1.0
        for j in range(n):
            if j != i:
                a = x - points[i]['x']
                b = points[i]['x'] - points[j]['x']
                F *= a / b
        res += F * points[i]['y']
    return res
```

- ##### метод Ньютона
```
def Newton(x, points):
    res = points[0]['y']
    n = len(points)
    q = (x - points[0]['x']) / (points[1]['x'] - points[0]['x'])
    for i in range(1, n):
        F = 0
        for j in range(i + 1):
            d = 1
            for k in range(j + 1):
                if k != j:
                    d *= (points[j]['x'] - points[k]['x'])
            F += points[j]['y'] / d
        for k in range(i):
            F *= (x - points[k]['x'])
        res += F
    return res
```

-----

#### Задание 2 (решение ОДУ)
- ##### метод Эйлера 
```
def eiler(f, startPoint, rightX, h):
    ans = [startPoint]
    n = 0
    while ans[n]['x'] < rightX - h / 2:
        newy = ans[n]['y'] + h * f(ans[n]['x'], ans[n]['y'])
        newx = ans[n]['x'] + h
        point = {'x': newx, 'y': newy}
        ans.append(point)
        n += 1
    return ans
#если в задании есть функция, вбивать её сюда 
def f(x, y):
    return x ** 2 - 2 * y
```

- ##### метод Эйлера усовершенствованный
```
def eilerPlus(f, startPoint, rightX, h):
    ans = [startPoint]
    n = 0
    h2 = h / 2
    while ans[n]['x'] < rightX - h / 2:
        p = ans[n]
        y = p['y'] + h2 * f(p['x'], p['y'])
        x = p['x'] + h2
        newpoint = {'x': p['x'] + h, 'y': p['y'] + h * f(x, y)}
        ans.append(newpoint)
        n += 1
    return ans
#если в задании есть функция, вбивать её сюда
def f(x, y):
    return x ** 2 - 2 * y
```

- ##### метод Адамса
```
def adams(f, startPoint1, startPoint2, strartPoint3, rightX, h):
    ans = [startPoint1, startPoint2, strartPoint3]
    n = 2
    h2 = h / 2
    while ans[n]['x'] < rightX - h / 2:
        k1 = f(ans[n]['x'], ans[n]['y']) * h
        k2 = f(ans[n - 1]['x'], ans[n]['y']) * h
        k3 = f(ans[n - 2]['x'], ans[n]['y']) * h
        delt = (23 * k1 - 16 * k2 + 5 * k3) / 12
        newpoint = {'x': ans[n]['x'] + h, 'y': ans[n]['y'] + delt}
        ans.append(newpoint)
        n += 1
    return ans
#если в задании есть функция, вбивать её сюда 
def f(x, y):
    return x ** 2 - 2 * y
```

- ##### метод Милна
```
def Miln(f, startPoint1, startPoint2, strartPoint3, rightX, h):
    ans = [startPoint1, startPoint2, strartPoint3]
    n = 2
    h2 = h / 2
    while ans[n]['x'] < rightX - h / 2:
        k1 = f(ans[n]['x'], ans[n]['y']) * h
        k2 = f(ans[n - 1]['x'], ans[n]['y']) * h
        k3 = f(ans[n - 2]['x'], ans[n]['y']) * h
        delt = (23 * k1 - 16 * k2 + 5 * k3) / 12
        newpoint = {'x': ans[n]['x'] + h, 'y': ans[n]['y'] + delt}
        ans.append(newpoint)
        n += 1
    return ans
#если в задании есть функция, вбивать её сюда
def f(x, y):
    return x ** 2 - 2 * y
```
